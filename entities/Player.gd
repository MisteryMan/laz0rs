extends "res://engine/movement.gd"

const SPEED = 80


func _physics_process(delta):
	controls_loop()
	movement_loop()
	

func controls_loop():
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	moveDir.y = -int(UP) + int(DOWN)