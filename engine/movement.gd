extends KinematicBody2D

var moveDir = Vector2(0,0)
const SPEED = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	
	pass
	
func movement_loop():
	var motion = moveDir.normalized() * SPEED
	move_and_slide(motion, Vector2(0,0))